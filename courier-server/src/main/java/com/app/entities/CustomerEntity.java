package com.app.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "customer")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CustomerEntity extends BaseEntity {

	@Column(length = 20)
	private String firstName;
	@Column(length = 20)
	private String lastName;
	@Column(length = 20, unique = true)
	private String email;
	@Column(length = 35)
	private String password;
	
//	@OneToMany(mappedBy = "customerFeedback", cascade = CascadeType.ALL)
//	private List<FeedbackEntity> feedbacks;
//	
//	@OneToMany(mappedBy = "customerCouriers", cascade = CascadeType.ALL)
//	private List<CourierEntity> couriers;
	
	
}
