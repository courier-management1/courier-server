package com.app.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

//CREATE TABLE employees(
//empId int primary key auto_increment,
//empName VARCHAR(40) NOT NULL,
//email VARCHAR(40) NOT NULL,
//password VARCHAR(30) NOT NULL,
//mobileNo bigint NOT NULL,
//addressId int NOT NULL,
//designation VARCHAR(30) NOT NULL,
//branchId int NOT NULL,
//createdTimestamp timestamp DEFAULT CURRENT_TIMESTAMP,
//FOREIGN KEY(addressId) REFERENCES address(addressId) ON DELETE CASCADE ON UPDATE CASCADE
//,
//FOREIGN KEY(branchId) REFERENCES branches(branchId) ON DELETE CASCADE ON UPDATE CASCADE
//);


@Entity
@Table(name = "employee")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EmployeeEntity extends BaseEntity{
	
	@Column(length = 20)
	private String firstName;
	@Column(length = 20)
	private String lastName;
	@Column(length = 20, unique = true)
	private String email;
	@Column(length = 35)
	private String password;
	private int mobileNo;
	@Column(length = 15)
	private String role;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "branch_id")
	private BranchEntity branch;
	
	@OneToMany(mappedBy = "allotedToDeliveryBoy", cascade = CascadeType.ALL) //or persist
	private List<CourierEntity> couriers;

}
